<?php

namespace Po4e4ka\SymfonyAR;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

trait QueryTrait
{
    private static function getRepoAndAlias(): array
    {
        $doctrine = DoctrineInstance::getDoctrine();
        if ($doctrine === null) {
            throw new \LogicException(
                "Доктрина не объявлена. Добавьте в конструктор\n" .
                'вызываемого класса DoctrineInstance::setDoctrine(ManagerRegistry $doctrine);');
        }
        $repo = $doctrine->getRepository(persistentObject: self::class);
        $alias = strtolower(basename(str_replace('\\', '/', self::class)));
        return [$repo, $alias];
    }

    public static function query($customAlias = null): QueryBuilder
    {
        [$repo, $alias] = self::getRepoAndAlias();
        return $repo->createQueryBuilder(alias: $customAlias ?? $alias);

    }

    public static function find(int|string $id): ?self
    {
        [$repo, $alias] = self::getRepoAndAlias();
        return $repo->find($id);
    }

    public static function findOrCreate(int|string $id): ?self
    {
        [$repo, $alias] = self::getRepoAndAlias();
        $entity = $repo->find($id);
        if ($entity)
            return $entity;
        return new self();
    }

    public static function findOneByOrCreate(array $criteria): ?self
    {
        $entity = self::findOneBy($criteria);
        if ($entity)
            return $entity;
        return new self();
    }

    /**
     * @return self[]|null
     */
    public static function findAll($orderByColumn = 'id', $direction = 'DESC')
    {
        [$repo, $alias] = self::getRepoAndAlias();
        /** @var QueryBuilder $queryResult */
        $queryResult = $repo->createQueryBuilder(alias: $alias)->orderBy($alias . '.' . $orderByColumn, $direction)->getQuery()->getResult();

        if (count($queryResult) === 0) {
            return null;
        }
        return $queryResult;
    }

    public static function findOneBy(array $criteria): ?self
    {
        [$repo, $alias] = self::getRepoAndAlias();
        return $repo->findOneBy($criteria);
    }

    /**
     * @param array $criteria
     * @return self[]|null
     */
    public static function findBy(array $criteria): ?array
    {
        /** @var ObjectRepository $repo  */
        [$repo, $alias] = self::getRepoAndAlias();
        return $repo->findBy($criteria);
    }

    /**
     * @param int $countLast
     * @return self[]|null
     */
    public static function findLastArray(int $countLast, $orderBy = 'id'): ?array
    {
        /**  @var ObjectRepository $repo */
        [$repo, $alias] = self::getRepoAndAlias();
        return $repo->createQueryBuilder(alias: $alias)
            ->setMaxResults($countLast)
            ->addOrderBy($alias . '.' . $orderBy, 'DESC')
            ->getQuery()->getResult();
    }

    public static function allCount(): int
    {
        /**  @var ServiceEntityRepository $repo */
        [$repo, $alias] = self::getRepoAndAlias();
        return $repo->createQueryBuilder($alias)->select('COUNT(' . $alias . '.id)')
            ->getQuery()->getSingleScalarResult();
    }
    
    public function save(): bool
    {
        $manager = DoctrineInstance::getDoctrine()->getManager();
        $manager->persist($this);
        $manager->flush();
    }    
}