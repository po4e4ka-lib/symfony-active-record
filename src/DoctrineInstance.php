<?php

namespace Po4e4ka\SymfonyAR;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

class DoctrineInstance
{
    static private ?ManagerRegistry $doctrine = null;

    /**
     * @return ManagerRegistry|null
     */
    static public function getDoctrine(): ?ManagerRegistry
    {
        return self::$doctrine;
    }

    /**
     * @param ManagerRegistry $doctrine
     */
    static public function setDoctrine(ManagerRegistry $doctrine): void
    {
        if (self::$doctrine === null) {
            self::$doctrine = $doctrine;
        }
    }

    /**
     * @return ObjectManager
     */
    static public function getManager(): ObjectManager
    {
        return self::$doctrine->getManager();
    }
}